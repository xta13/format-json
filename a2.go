package main
import (
  "os"
  "fmt"
  "io/ioutil"
  "strings"
)
//reference http://www.cs.sfu.ca/CourseCentral/383/tjd/go-intro.html
type Token struct{
  identity string
  value string
}
func is_true(data string, position int) bool{
  if len(data)-position >=4{
    if data[position:position+4]=="true"{
      return true
    }
    return false
  }
  return false
}

func is_false(data string, position int) bool{
  if len(data)-position >=5{
    if data[position:position+5]=="false"{
      return true
    }
    return false
  }
  return false
}

func is_null(data string, position int) bool{
  if len(data)-position >=4{
    if data[position:position+4]=="null"{
      return true
    }
    return false
  }
  return false
}

func is_empty_token(token Token) bool{
  return token==Token{identity:"",value:""}
}

func get_string(data string, position int) (string , int){
  index:=position+1
  for index < len(data)-1{
    if data[index] == '"'{
      return data[position:index+1], index+1
    }else if data[index] == 92{ //reference: http://www.asciitable.com/
        switch data[index+1]{
        case 117:
          index = index + 6
        case 34, 92, 47, 98, 102, 110, 114, 116: //reference: http://www.asciitable.com/
          index = index + 2
        default:
          fmt.Printf("In function get_string()")
        }
        continue
    }
    index++
  }
  return data[position:index+1],index+1
}
func is_part_of_number( value uint8) bool{
  return value == '0' || value == '1' || value == '2' || value == '3' || value == '4' || value == '5' ||
         value == '6' || value == '7' || value == '8' ||value == '9' ||value == 'e' ||value == 'E' ||value == '+' ||value == '-' ||value == '.'
}
func get_number(data string, position int) (string, int){
  index:=position+1
  for index < len(data){
    if !is_part_of_number(data[index]){
      return data[position:index], index
    }
    index++
  }
  return data[position:index], index
}

func generateToken( data string ) []Token {
  result:=[]Token{}
  var token Token
  position:=0
  for position < len(data){
    token = Token{identity:"",value:""}
    if data[position]== '{'{
      token.identity="brace"
      token.value="{"
    }else if data[position]=='}'{
      token.identity="brace"
      token.value="}"
    }else if data[position]=='['{
      token.identity="bracket"
      token.value="["
    }else if data[position]==']'{
      token.identity="bracket"
      token.value="]"
    }else if data[position]==':'{
      token.identity="colon"
      token.value=":"
    }else if data[position]==','{
      token.identity="comma"
      token.value=","
    }else if is_true(data,position){
      token.identity="bool"
      token.value="true"
      position= position+4
      result=append(result,token)
      continue
    }else if is_false(data,position){
      token.identity="bool"
      token.value="false"
      position= position+5
      result=append(result,token)
      continue
    }else if is_null(data,position){
      token.identity="bool"
      token.value="null"
      position= position+4
      result=append(result,token)
      continue
    }else if data[position]=='"'{
      token.identity="string"
      token.value, position = get_string( data, position)
      result=append(result,token)
      continue
    }else if is_part_of_number(data[position]){
      token.identity="number"
      token.value, position = get_number(data, position)
      result=append(result,token)
      continue
    }
    if !is_empty_token(token){
      result=append(result,token)
    }
    position++
  }
  return result
}

//putting token together and add <span> tag
func add_span( one_token Token) string{
  result:=""
  identity := one_token.identity
  value := one_token.value
  switch identity{
    case "brace":
     result= "<span style=\"color:green\">"+value+"</span>"
    case "bracket":
     result= "<span style=\"color:red\">"+value+"</span>"
    case "bool":
     result= "<span style=\"color:rgb(153,0,0)\">"+value+"</span>"
    case "colon":
     result= "<span style=\"color:blue\">"+value+"</span>"
    case "comma":
     result= "<span style=\"color:rgb(153,153,0)\">"+value+"</span>"
    case "number":
     result= "<span style=\"color:orange\">"+value+"</span>"
    case "string":
     index:=0
     start:=0
     for index < len(value)-1 {
       if value[index] == 92{ //reference: http://www.asciitable.com/
        switch value[index+1]{
        case 117:
         if index+6 <= len(value){
          if index-start > 0{
              result = result + "<span style=\"color:purple\">" + value[start:index]+ "</span>" + "<span style=\"color:rgb(0,0,102)\">" + value[index:index+6]+"</span>"
          }else{ //index== start
              result = result + "<span style=\"color:rgb(0,0,102)\">" + value[index:index+6]+"</span>"
          }
         }
          index = index + 6
          start=index
        case 92, 47, 98, 102, 110, 114, 116: //reference: http://www.asciitable.com/
          if index+2 <= len(value){
            if index-start > 0{
              result = result + "<span style=\"color:purple\">" + value[start:index]+ "</span>" + "<span style=\"color:rgb(0,0,102)\">" + value[index:index+2]+"</span>"
            }else{ //index==start
              result = result + "<span style=\"color:rgb(0,0,102)\">" + value[index:index+2]+"</span>"
            }
          }
          index = index + 2
          start=index
        default:
          if value[index+1:index+7]=="&quot;"{
            if index+7 <= len(value){
              if index-start > 0{
                result = result + "<span style=\"color:purple\">" + value[start:index]+ "</span>" + "<span style=\"color:rgb(0,0,102)\">" + value[index:index+7]+"</span>"
              }else{ //index==start
                result = result + "<span style=\"color:rgb(0,0,102)\">" + value[index:index+7]+"</span>"
              }
            }
            index = index + 7
            start=index
          }else{
            fmt.Print("In function add_span()")
            index++
          }
        }
        continue
      } 
      index++
     }
     if start < len(value){
       result=result + "<span style=\"color:purple\">" + value[start:len(value)]+ "</span>"
     }else{
       fmt.Print("nothing remain in string")
     }
    default:
     result="<span></span>"
  }
  return result
}
func pre_new_line_needed(token []Token, index int) bool{
  if index == 0{
    return false
  }
  if index > len(token)-1{
    return false
  } 
  if token[index].identity!="brace"{
    return false
  }
  if len(token[index-1].value) < 2{
    return true
  } 
  last_index:=len(token[index-1].value)-1
  second_last_index:=len(token[index-1].value)-2
  if !(token[index-1].value[second_last_index] == 92 && token[index-1].value[last_index] == 110){
    return true
  }
  return false
}

func generateHTML(token []Token) string{
  result:=""
  index:=0
  indent:=0
  tab_needed:=false
  next_line_for_comma_needed:=true
  for index < len(token){
    //replace symbols 
    temp_string:= strings.Replace(token[index].value,"&", "&amp;",-1)
    temp_string = strings.Replace(temp_string,"<", "&lt;",-1)
    temp_string = strings.Replace(temp_string,">", "&gt;",-1)
    temp_string = strings.Replace(temp_string,"\"", "&quot;",-1)
    temp_string = strings.Replace(temp_string,"'", "&apos;",-1)
    token[index].value = temp_string
    if token[index].value == "{"{
      next_line_for_comma_needed=true
      indent++
      temp:=0
      for temp < indent-1 {
        token[index].value = "\t" + token[index].value
        temp++
      }
      if pre_new_line_needed(token,index){
        token[index].value="\n"+token[index].value
      }
      token[index].value = token[index].value + "\n"
      tab_needed=true
    }else if( token[index].value == "}") {
      next_line_for_comma_needed=false
      temp:=0
      for temp < indent-1{
        token[index].value = "\t" + token[index].value
        temp++
      }
      indent--
      if pre_new_line_needed(token,index){
        token[index].value="\n"+token[index].value
      }
      token[index].value = token[index].value + "\n"
      tab_needed=true
    }else if token[index].identity == "colon"{
      token[index].value = " " + token[index].value + " "
      tab_needed=false
    }else if token[index].identity == "comma"{
      if next_line_for_comma_needed{
        token[index].value = token[index].value + "\n"
        tab_needed=true
      }
    }else{
      if tab_needed{
        temp:=0
        for temp < indent{
          token[index].value = "\t" + token[index].value
          temp++
        }
        tab_needed=false
      }
      if token[index].identity == "bracket"{
        if token[index].value == "["{
          next_line_for_comma_needed=false
        }else if token[index].value == "]"{
          next_line_for_comma_needed=true
        }
     }
    }
    result = result + add_span(token[index])
    index++
  }
  return result
}
func add_html_header(body_content string) string{
  result:=""
  doc_type_tag:="<!DOCTYPE html>\n"
  html_open_tag:="<html lang=\"en\">\n"
  html_close_tag:="</html>"
  head_tags:="\t<head>\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\t\t<meta charset=\"UTF-8\">\n\t\t<title>JsonHTML</title>\n\t</head>\n"
  body_open_tag:="\t<body>\n"
  body_close_tag:="\n\t</body>\n"
  warp_open_tag:="<span style=\"font-family:monospace; white-space:pre\">"
  warp_close_tag:="</span>"
  result=doc_type_tag + html_open_tag + head_tags + body_open_tag + warp_open_tag + body_content + warp_close_tag + body_close_tag + html_close_tag
  return result
}

func main() {
  if len(os.Args) < 2{
    panic("too less command-line argument")
  }
  fileName := os.Args[1]
  byte,err:=ioutil.ReadFile(fileName)
  if err != nil{
    fmt.Println("error when reading file")
    return
  }
  data := string(byte)
  //JSON scanner
  tokens :=generateToken(data)
  // colorizer/formatter
  HTML_body_content := generateHTML(tokens)
  complete_HTML := add_html_header(HTML_body_content)
  fmt.Printf(complete_HTML)
}
